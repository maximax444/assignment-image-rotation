#include "images.h"
#include <stdlib.h>
//структуры фотки и пикселя, методы для работы с фотками



// размер строки
uint32_t image_size_row(struct image const* img){
    return sizeof (struct pixel) * img->width;
}

// размер фотки
uint32_t image_size_all(struct image const* img){
    return sizeof(struct pixel) * img->width * img->height;
}

// получаем пиксель
struct pixel get_pixel(const struct image img, const uint64_t x, const uint64_t y){
    return img.data[x + y * img.width];
}

// создаём фотку
struct image* create_image(struct image* img, uint64_t width, uint64_t height){
    img->height = height;
    img->width = width;
    img->data = malloc(image_size_all(img));
    return img;
}

// освобождаем память
void free_image(struct image *img){
    free(img->data);
}
