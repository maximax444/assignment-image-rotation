#include "files_stat.h"
//методы открывающие файлы 

bool open_file(FILE** myFile, const char* myFilename, const char* openMode) {
    *myFile = fopen(myFilename, openMode);
    return (*myFile != NULL);
}

bool close_file(FILE** myFile) {
    int close_stat = fclose(*myFile);
    return (close_stat == 0);
}
