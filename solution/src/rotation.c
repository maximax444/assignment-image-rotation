#include "rotation.h"
#include <stdlib.h>
// поворот фотки
struct image rotate(struct image myImage) {
    struct image rotated = {0, 0, NULL};
    rotated.width = myImage.height;
    rotated.height = myImage.width;
    rotated.data = malloc(image_size_all(&myImage));
    if (rotated.data == NULL) {
        return (struct image) {0, 0, NULL};
    }
    for (size_t y = 0; y < rotated.height; y++) {
        for (size_t x = 0; x < rotated.width; x++) {
            rotated.data[y * rotated.width + x] = get_pixel(myImage, y, myImage.height - x - 1);
        }
    }
    return rotated;
}
