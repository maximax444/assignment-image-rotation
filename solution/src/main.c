#include "bmp.h"
#include "files_stat.h"
#include "rotation.h"

#include <stdio.h>

int main( int argc, char** argv ) {

    

    

    // Проверяем аргументы
    if (argc < 3) {
        fprintf(stderr, "Не все аргументы введены");
        return -1;
    }

    // Берём имена файлов из аргументов
    char* input_filename = argv[1];
    char* output_filename = argv[2];


    // Пробуем открыть файлы
    FILE* file_input = NULL;
    FILE* file_output = NULL;
    bool file_input_opened = open_file(&file_input, input_filename, "rb");
    bool file_output_opened = open_file(&file_output, output_filename, "wb");

    // Проверяем успешность открытия
    if (file_input_opened) {
    	if (!file_output_opened) {
    	    close_file(&file_input);
    	    fprintf(stderr, "Output file не существует или нет доступа \n");
    	    return -1;
    	} else {
    		fprintf(stdout, "Input&output files открыты \n");
    	}
    } else {
        if (!file_output_opened) {
            fprintf(stderr, "Input&output files не существуют или нет доступа \n");
        }
        else {
            close_file(&file_output);
            fprintf(stderr, "Input file не существует или нет доступа \n");
        }
        return -1;
    }



    // Чтение файла
    struct image my_image = {0,0,NULL};
    if (from_bmp(file_input, &my_image)!= ROK) {
        fprintf(stderr, "Указан не .bmp файл\n");
        return -1;
    }
    fprintf(stdout, "Файл прочитан \n");


    // Поворот картинки
    struct image rotated_image = rotate(my_image);
    free_image(&my_image);
    // запись повернутого файла, очистка памяти, закрытие файлов
    if (to_bmp(file_output, &rotated_image) != WOK) {
        fprintf(stderr, "Ошибка конвертации в .bmp \n");
        return -1;
    }
    free_image(&rotated_image);
    
    close_file(&file_input);
    close_file(&file_output);
    fprintf(stdout, "Конвертация выполнена, файлы закрыты, работа программы окончена \n");

    return 0;
}
