#include "bmp.h"
#include "files_stat.h"

#include <stdlib.h>
//методы для работы с bmp
static const uint32_t bfType = 0x4D42;
static const uint32_t biSize = 40;
static const size_t biBitCount = 24;


// Структура шапки bmp
struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

// расчёт отступа ширины
size_t bmp_padding(uint64_t width){
    return 4 - (width * 3) % 4;
}

// Заполнение отступов
static void fill_padding(struct image *img, FILE *out){
    uint8_t myPadding = bmp_padding(img->width);
    uint8_t paddings[4];
    for (size_t i = 0; i < myPadding; i++) {
        paddings[i] = 0;
    }
    fwrite(paddings, myPadding, 1, out);
}

// Определение размера
uint32_t image_bmp_size(struct image *img){
    return (image_size_all(img) + bmp_padding(img->width)) * img->height;
}

// создание шапки
struct bmp_header create_header(struct image *img) {
    struct bmp_header h;

    h.bfType = bfType;
    h.bfileSize = sizeof(struct bmp_header) + image_bmp_size(img);
    h.bfReserved = 0;
    h.bOffBits = sizeof(struct bmp_header);
    h.biSize = biSize;
    h.biWidth = img->width;
    h.biHeight = img->height;
    h.biPlanes = 1;
    h.biBitCount = biBitCount;
    h.biCompression = 0;
    h.biSizeImage = image_bmp_size(img);
    h.biXPelsPerMeter = 0;
    h.biYPelsPerMeter = 0;
    h.biClrUsed = 0;
    h.biClrImportant = 0;
    return h;
}

// Читаем  исходную фотку
enum read_stat from_bmp(FILE *in, struct image *img){
    struct bmp_header header = {0};
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        fprintf(stderr, "Got an error while reading header\n");
        return RINVALID_HEADER;
    }
    if (header.biBitCount != 24 || !in){
        fprintf(stderr, "Not supported bit count (not 24)\n");
        return RINVALID_COUNT;
    }
    if (header.bfType != bfType){
        return RINVALID_SIGNATURE;
    }

    img = create_image(img, header.biWidth, header.biHeight);

    for(int i = 0; i < img->height; i++){
        if(fread(img->data + i * img->width, image_size_row(img), 1, in) != 1){
            fprintf(stderr, "Got an error while reading\n");
            return RINVALID_READ;
        }
        if (fseek(in, bmp_padding(img->width), SEEK_CUR)){
            return RNULL;
        }
    }
    return ROK;
}

// Создаем фотку
enum write_stat to_bmp(FILE *out, struct image *img){
    struct bmp_header header = create_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1) {
        return WERROR;
    }
    fseek(out, header.bOffBits, SEEK_SET);
    if (img->data != NULL) {
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->data + i * img->width, image_size_row(img), 1, out);
            fill_padding(img, out);
        }
    }
    return WOK;
}
