#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGES_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGES_H

#include <stdint.h>
//структуры фотки и пикселя, методы для работы с фотками

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};


uint32_t image_size_row(struct image const* img);
uint32_t image_size_all(struct image const* img);
struct pixel get_pixel(const struct image img, const uint64_t x, const uint64_t y);


struct image* create_image(struct image* img, uint64_t width, uint64_t height);
void free_image(struct image *img);


#endif 
