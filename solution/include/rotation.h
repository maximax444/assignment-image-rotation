#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATION_H

#include "images.h"

struct image rotate(struct image myImage);

#endif 
