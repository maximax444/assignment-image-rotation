#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "images.h"
#include <stdint.h>
#include <stdio.h>
//методы для работы с bmp
enum write_stat {
    WOK = 0,
    WERROR
};
enum read_stat {
    ROK = 0,
    RINVALID_SIGNATURE,
    RINVALID_HEADER,
    RINVALID_READ,
    RINVALID_COUNT,
    RNULL
};
enum write_stat to_bmp(FILE *out, struct image *img);
enum read_stat from_bmp(FILE *in, struct image *img);


#endif 
