#ifndef FILE_STAT_H
#define FILE_STAT_H
#include <stdbool.h>
#include <stdio.h>
//методы открывающие файлы 
bool open_file(FILE** myFile, const char* myFilename, const char* openMode);
bool close_file(FILE** myFile);


#endif
